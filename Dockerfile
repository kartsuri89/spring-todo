# syntax=docker/dockerfile:1

FROM eclipse-temurin:17-jdk-jammy
EXPOSE 9090
ADD target/spring-todo-docker.jar spring-todo-docker.jar
ENTRYPOINT [ "java", "-jar", "/spring-todo-docker.jar" ]