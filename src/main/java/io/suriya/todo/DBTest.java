package io.suriya.todo;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBTest {

	static String url = "jdbc:sqlite:/home/kartsuri89/sandbox/sqlite.db";

	public static void main(String[] args) {
		connect();
		createTable();
		insert("suriya", 6.0);
		insert("dhanya", 8.0);
		selectAll();
	}
	public static void connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);

			System.out.println("Connection to SQLite has been established.");

			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				System.out.println("The driver name is " + meta.getDriverName());
				System.out.println("A new database has been created.");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	public static void createTable() {
		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS employees (\n" + " id integer PRIMARY KEY,\n"
				+ " name text NOT NULL,\n" + " capacity real\n" + ");";

		try (Connection conn = DriverManager.getConnection(url)) {
			Statement stmt = conn.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void insert(String name, double capacity) {
		String sql = "INSERT INTO employees(name, capacity) VALUES(?,?)";

		try (Connection conn = DriverManager.getConnection(url)) {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setDouble(2, capacity);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void selectAll() {
		String sql = "SELECT * FROM employees";

		try (Connection conn = DriverManager.getConnection(url)) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getDouble("capacity"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
