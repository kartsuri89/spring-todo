package io.suriya.todo.controller;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	Logger log = LoggerFactory.getLogger(HelloController.class);

	@GetMapping("/testlog")
	public ResponseEntity<String> getMessage() {
		IntStream.range(1, 100).forEach(a -> {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				log.error("Interuppted exp : {}", e.getMessage());
				Thread.currentThread().interrupt();
			}
			log.info(String.valueOf(a));
		});
		log.info("inside getMessage: {}", new Date());
		return new ResponseEntity<>("Hello World!", HttpStatus.OK);
	}

}
